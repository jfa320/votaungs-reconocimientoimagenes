package extractor;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExtractorDNITxt implements ExtractorDatosDNI{

	@Override
	public String extraerDatosDNI(String imagen)   {
		
		File file = new File(imagen); 
		String dni = "";
		String lineaActual;
		try {
			BufferedReader br = new BufferedReader(new FileReader(file));
			while ((lineaActual = br.readLine()) != null) {
				dni= lineaActual;
			}
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return dni;
		
	}

}
